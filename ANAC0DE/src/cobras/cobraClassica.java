package cobras;

import entidades.Inicio;

import java.awt.*;
import java.awt.event.*;
import java.util.Random;

import javax.swing.*;

public class cobraClassica extends JPanel implements KeyListener, ActionListener {
    private int[] tamanhoCobraX = new int[750];
    private int[] tamanhoCobraY = new int[750];

    private boolean esquerda = false;
    private boolean direita = false;
    private boolean cima = false;
    private boolean baixo = false;
    private static boolean fim = false;

    private int tamanhoCobra = 3;

    private Timer timer;
    private int atraso = 100;

    private int[] posXSimples = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750,
            775, 800, 825, 850};
    private int[] posXGrande = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750,
            775, 800, 825, 850};
    private int[] posXDiminui = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750,
            775, 800, 825, 850};
    private int[] posXBomba = {25, 50, 75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625, 650, 675, 700, 725, 750,
            775, 800, 825, 850};

    private int[] posYFruta = {75, 100, 125, 150, 175, 200, 225, 250, 275, 300, 325,
            350, 375, 400, 425, 450, 475, 500, 525, 550, 575, 600, 625};

    private Random random = new Random();
    private int xpos = random.nextInt(34);
    private int ypos = random.nextInt(23);

    private Random frutaAleatoria = new Random();
    private int apareceFruta = frutaAleatoria.nextInt(20);

    private int placar = 0;
    public int cronometro;
    private int movimentos = 0;

    private ImageIcon frutaSimples;
    private ImageIcon frutaGrande;
    private ImageIcon frutaDiminui;
    private ImageIcon frutaBomba;
    private ImageIcon bloco;
    private ImageIcon imagemTitulo;
    private ImageIcon snakeimage;

    public cobraClassica() {
        fim = false;
        addKeyListener(this);
        cronometro = 0;
        setFocusable(true);
        setFocusTraversalKeysEnabled(false);
        timer = new Timer(atraso, this);
        timer.start();
    }

    public void jogoAcaba(Graphics g) {
        fim = true;
        direita = false;
        esquerda = false;
        cima = false;
        baixo = false;

        g.setColor(Color.red);
        g.setFont(new Font("arial", Font.BOLD, 50));
        g.drawString("Tu perdeu, otário", 240, 250);

        g.setFont(new Font("arial", Font.BOLD, 20));
        g.drawString("Aperte espaço pra voltar para o MENU ", 240, 320);
    }

    public int getPlacar() {
        return placar;
    }

    public void paint(Graphics g) {

        if (movimentos == 0) {
            tamanhoCobraX[2] = 50;
            tamanhoCobraX[1] = 75;
            tamanhoCobraX[0] = 100;

            tamanhoCobraY[2] = 100;
            tamanhoCobraY[1] = 100;
            tamanhoCobraY[0] = 100;
        }

        g.setColor(Color.green);
        g.drawRect(24, 10, 851, 55);

        imagemTitulo = new ImageIcon("componentes/titulo.jpg");
        imagemTitulo.paintIcon(this, g, 25, 11);

        g.setColor(Color.green);
        g.drawRect(24, 74, 851, 577);

        g.setColor(Color.white);
        g.fillRect(25, 75, 850, 575);

        g.setColor(Color.WHITE);
        g.setFont(new Font("calibri", Font.BOLD, 14));
        g.drawString("Pontuação: " + placar, 90, 40);


        snakeimage = new ImageIcon("componentes/classica.jpg");
        snakeimage.paintIcon(this, g, tamanhoCobraX[0], tamanhoCobraY[0]);

        for (int a = 0; a < tamanhoCobra; a++) {
            snakeimage = new ImageIcon("componentes/classica.jpg");
            snakeimage.paintIcon(this, g, tamanhoCobraX[a], tamanhoCobraY[a]);
        }

        if (apareceFruta <= 5) {
            frutaSimples = new ImageIcon("componentes/fruta.png");
            if ((posXSimples[xpos] == tamanhoCobraX[0] && posYFruta[ypos] == tamanhoCobraY[0])) {
                placar++;
                tamanhoCobra++;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);

                apareceFruta = frutaAleatoria.nextInt(10);
            } else if (cronometro >= 100) {
                cronometro = 0;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);
                apareceFruta = frutaAleatoria.nextInt(10);
            }
            frutaSimples.paintIcon(this, g, posXSimples[xpos], posYFruta[ypos]);
        } else if (apareceFruta > 5 && apareceFruta <= 10) {
            frutaGrande = new ImageIcon("componentes/grandeFruta.png");
            if ((posXGrande[xpos] == tamanhoCobraX[0] && posYFruta[ypos] == tamanhoCobraY[0])) {
                placar += 2;
                tamanhoCobra++;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);

                apareceFruta = frutaAleatoria.nextInt(10);
            } else if (cronometro >= 100) {
                cronometro = 0;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);
                apareceFruta = frutaAleatoria.nextInt(10);
            }
            frutaGrande.paintIcon(this, g, posXGrande[xpos], posYFruta[ypos]);
        } else if (apareceFruta > 10 && apareceFruta <= 15) {
            frutaDiminui = new ImageIcon("componentes/diminuiFruta.png");
            if ((posXBomba[xpos] == tamanhoCobraX[0] && posYFruta[ypos] == tamanhoCobraY[0])) {
                tamanhoCobra = 3;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);

                apareceFruta = frutaAleatoria.nextInt(10);
            } else if (cronometro >= 100) {
                cronometro = 0;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);
                apareceFruta = frutaAleatoria.nextInt(10);
            }
            frutaDiminui.paintIcon(this, g, posXDiminui[xpos], posYFruta[ypos]);
        } else {
            frutaBomba = new ImageIcon("componentes/bombaFruta.png");
            if ((posXDiminui[xpos] == tamanhoCobraX[0] && posYFruta[ypos] == tamanhoCobraY[0])) {
                tamanhoCobra = 0;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);
                apareceFruta = frutaAleatoria.nextInt(10);
            } else if (cronometro >= 100) {
                cronometro = 0;
                xpos = random.nextInt(34);
                ypos = random.nextInt(23);
                apareceFruta = frutaAleatoria.nextInt(10);
            }
            frutaBomba.paintIcon(this, g, posXDiminui[xpos], posYFruta[ypos]);
        }

        bloco = new ImageIcon("componentes/bloco.jpg");

        for (int i = 200; i <= 650; i += 450) {
            for (int j = 200; j <= (25 * 12 + 200); j += 25) {
                bloco.paintIcon(this, g, i, j);
            }
        }


        if (tamanhoCobra <= 0) {
            jogoAcaba(g);
        }
        for (int i = 200; i <= 650; i += 450) {
            for (int j = 200; j <= (25 * 12 + 200); j += 25) {
                if (i == tamanhoCobraX[0] && j == tamanhoCobraY[0]) {
                    jogoAcaba(g);
                }
            }
        }
        for (int b = 1; b < tamanhoCobra; b++) {
            if (tamanhoCobraX[b] == tamanhoCobraX[0] && tamanhoCobraY[b] == tamanhoCobraY[0]) {
                jogoAcaba(g);
            }
        }

        try {
            Robot robot = new Robot();
            if(!fim){
                robot.mouseMove(100, 100);
            }
        }catch (AWTException p) {
            System.out.println("Low level input control is not allowed " + p.getMessage());
        }

        g.dispose();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        timer.start();
        cronometro++;

        try{
            Thread.sleep(1000/60);
        }
        catch(InterruptedException k){
            //TODO
        }

        if (direita) {
            for (int r = tamanhoCobra - 1; r >= 0; r--) {
                tamanhoCobraY[r + 1] = tamanhoCobraY[r];
            }
            for (int r = tamanhoCobra; r >= 0; r--) {
                if (r == 0) {
                    tamanhoCobraX[r] = tamanhoCobraX[r] + 25;
                } else {
                    tamanhoCobraX[r] = tamanhoCobraX[r - 1];
                }
                if (tamanhoCobraX[r] > 850) {
                    tamanhoCobraX[r] = 25;
                }
            }
            repaint();
        }
        if (esquerda) {
            for (int r = tamanhoCobra - 1; r >= 0; r--) {
                tamanhoCobraY[r + 1] = tamanhoCobraY[r];
            }
            for (int r = tamanhoCobra; r >= 0; r--) {
                if (r == 0) {
                    tamanhoCobraX[r] = tamanhoCobraX[r] - 25;
                } else {
                    tamanhoCobraX[r] = tamanhoCobraX[r - 1];
                }
                if (tamanhoCobraX[r] < 25) {
                    tamanhoCobraX[r] = 850;
                }
            }
            repaint();
        }
        if (cima) {
            for (int r = tamanhoCobra - 1; r >= 0; r--) {
                tamanhoCobraX[r + 1] = tamanhoCobraX[r];
            }
            for (int r = tamanhoCobra; r >= 0; r--) {
                if (r == 0) {
                    tamanhoCobraY[r] = tamanhoCobraY[r] - 25;
                } else {
                    tamanhoCobraY[r] = tamanhoCobraY[r - 1];
                }
                if (tamanhoCobraY[r] < 75) {
                    tamanhoCobraY[r] = 625;
                }
            }
            repaint();
        }
        if (baixo) {
            for (int r = tamanhoCobra - 1; r >= 0; r--) {
                tamanhoCobraX[r + 1] = tamanhoCobraX[r];
            }
            for (int r = tamanhoCobra; r >= 0; r--) {
                if (r == 0) {
                    tamanhoCobraY[r] = tamanhoCobraY[r] + 25;
                } else {
                    tamanhoCobraY[r] = tamanhoCobraY[r - 1];
                }
                if (tamanhoCobraY[r] > 625) {
                    tamanhoCobraY[r] = 75;
                }
            }
            repaint();
        }

    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {
        if (fim && e.getKeyCode() == KeyEvent.VK_SPACE) {
            System.gc();
            for (Window window : Window.getWindows()) {
                window.dispose();
            }
            fim = false;
            Inicio inicio = new Inicio();
        }
        if (!fim) {
            if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                movimentos++;
                direita = true;
                if (!esquerda) {
                    direita = true;
                } else {
                    direita = false;
                    esquerda = true;
                }
                cima = false;
                baixo = false;
            }
            if (movimentos != 0) {
                if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                    movimentos++;
                    esquerda = true;
                    if (!direita) {
                        esquerda = true;
                    } else {
                        esquerda = false;
                        direita = true;
                    }
                    cima = false;
                    baixo = false;
                }
            }

            if (e.getKeyCode() == KeyEvent.VK_UP) {
                movimentos++;
                cima = true;
                if (!baixo) {
                    cima = true;
                } else {
                    cima = false;
                    baixo = true;
                }
                esquerda = false;
                direita = false;
            }

            if (e.getKeyCode() == KeyEvent.VK_DOWN) {
                movimentos++;
                baixo = true;
                if (!cima) {
                    baixo = true;
                } else {
                    baixo = false;
                    cima = true;
                }
                esquerda = false;
                direita = false;
            }
        }


    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

}
