package entidades;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class Inicio extends JFrame{
    private JPanel inicio;
    private JRadioButton classicaRadioButton;
    private JRadioButton kittyRadioButton;
    private JRadioButton starRadioButton;
    private JButton sairButton;
    private JButton jogarButton;
    private JLabel titulo;


    public Inicio() {
        JFrame frame = new JFrame("Inicio");
        frame.setContentPane(new Inicio(frame).inicio);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public Inicio(JFrame frame) {
        jogarButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                if(classicaRadioButton.isSelected()){
                    telaDeJogo telaDeJogo = new telaDeJogo(1);
                }else if(kittyRadioButton.isSelected()){
                    telaDeJogo telaDeJogo = new telaDeJogo(2);
                }else if(starRadioButton.isSelected()){
                    telaDeJogo telaDeJogo = new telaDeJogo(3);
                }
                frame.dispose();
            }
        });
        sairButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
                System.exit(0);
            }
        });

    }

    public static void main(String[] args) {
        Inicio inicio = new Inicio();
    }


}
