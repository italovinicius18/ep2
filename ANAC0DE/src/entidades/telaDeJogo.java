package entidades;

import cobras.cobraClassica;
import cobras.cobraKitty;
import cobras.cobraStar;

import javax.swing.*;
import java.awt.*;


public class telaDeJogo extends JFrame{
    private JFrame tela;
    private int cobra;

    public telaDeJogo(int cobra) {
        cobraClassica classica = new cobraClassica();
        cobraKitty kitty = new cobraKitty();
        cobraStar star = new cobraStar();
        JFrame tela = new JFrame("Jogo");
        tela.setSize(905, 700);
        tela.setBackground(Color.green);
        tela.setResizable(false);
        tela.setVisible(true);
        tela.setLocationRelativeTo(null);
        tela.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        if(cobra==1){
            tela.add(classica);
        }
        if(cobra==2){
            tela.add(kitty);
        }
        if(cobra==3){
            tela.add(star);
        }
    }

}
