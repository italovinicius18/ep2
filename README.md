# ANAC0DE

- Turma A - Renato
- Ítalo Vinícius Pereira Guimarães
- 18/0102656

## Descrição

O jogo se baseia no clássico jogo da Snake, porém com algumas cobra e frutas diferentes, sendo inicialmente aberto um menu para escolher a cobra e jogar, equanto estiver jogando, controle a cobra a partir das teclas de direção. Se morrer, aperte espaço para voltar ao menu inicial.

## Tipos de Snakes

Deve ser implementado ao menos 3 tipos de Snakes:

- **Comum ![](ANAC0DE/componentes/classica.jpg) :** A Snake classica, sem habilidades especiais (Pode atravessar as bordas).
- **Kitty ![](ANAC0DE/componentes/kitty.jpg) :** Essa Snake tem as habilidades de atravessar as barreiras do jogo, mas não pode atravessar as bordas nem a si mesma.
- **Star ![](ANAC0DE/componentes/star.jpg) :** Recebe o dobro de pontos ao comer as frutas.

## Frutas

As frutas são elementos que aparecem aleatoriamente e são os objetivos das Snakes. As frutas desaparecem depois de um tempo fixo, ou seja, se aparecer uma fruta ruim, voce precisa esperar para que apareça outra fruta melhor.

- **Simple Fruit:** Fruta comum, dá um ponto e aumenta o tamanho da cobra.
  ![](ANAC0DE/componentes/fruta.png)
- **Bomb Fruit:** Essa fruta deve levar a morte da Snake.
  ![](ANAC0DE/componentes/bombaFruta.png)
- **Big Fruit:** Dá o dobro de pontos da Simple Fruit e aumenta o tamanho da cobra da mesma forma que a Simple Fruit.
  ![](ANAC0DE/componentes/grandeFruta.png)
- **Decrease Fruit:** Diminui o tamanho da cobra para o tamanho inicial, sem fornecer nem retirar pontos.
  ![](ANAC0DE/componentes/diminuiFruta.png)

## Pontos

Os pontos são calculados de acordo com as frutas coletadas.

## Dependências

O Jogo foi feito no Intellij IDEA 2019.02 e a versão do Java utilizado para o prjeto é a versão 11

    import javax.swing.JFrame;
    import javax.swing.ImageIcon;
    import javax.swing.JPanel;
    import javax.swing.Timer;
    import java.awt.*;
    import java.util.ArrayList;
    import java.awt.image.BufferedImage;
    import java.awt.event.*;
    import java.awt.image.*;
    import javax.imageio.ImageIO;
    import java.awt.event.KeyEvent;
    import java.awt.Color;
    import java.awt.Graphics;
    import java.awt.Font;
    import java.awt.event.ActionListener;
    import java.awt.event.ActionEvent;
    import java.awt.event.KeyListener;
    import java.util.Random;

## Como rodar o jogo ?

- O jogo pode ser iniciado a partir da abertura do projeto ANAC0DE (Anaconda+code) no Intellij e assim ser rodado
- Ou pode ser executado dentro da pasta ANAC0DE/ o comando:
```
java -jar ANAC0DE.jar
```
## Diagrama de classe

![](ANAC0DE/componentes/DiagramaDeClasseANAC0DE.png)
